# Service | Hello You

This is a basic web service which consumes and updates data from a database.

## Required Environment Configuration Variables

Required: 
- DATASOURCE_URL
- DATASOURCE_USER
- DATASOURCE_PASSWORD
- AWS_COGNITO_CLATSCH_USER_POOL_NAME
- AWS_COGNITO_CLATSCH_USER_POOL_ID
- AWS_REGION
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

## AWS Congito
You will need to configure a service role with access to AWS Cognito.  Use the id and access keys provided for the AWS environment variables.
- AWS_REGION
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY

The required OAuth2 links are configured in the Central Configuration repository and reference the environment variables for user pool name and user pool id.
- AWS_COGNITO_CLATSCH_USER_POOL_NAME
- AWS_COGNITO_CLATSCH_USER_POOL_ID

## Postgres
You will need to stand up a Postgres DBaaS from any provide you choose.  Fill in the following environment variable with the proper information:
- DATASOURCE_URL
- DATASOURCE_USER
- DATASOURCE_PASSWORD

